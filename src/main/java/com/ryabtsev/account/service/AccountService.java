package com.ryabtsev.account.service;

import com.ryabtsev.account.dto.AccountDto;
import com.ryabtsev.account.dto.MoneyTransferringDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
public class AccountService {
    @Autowired AccountTransactionalService accountTransactionalService;

    public AccountDto deposit(Long id, BigDecimal amount) {
        AccountDto deposit;

        while (true) {
            try {
                deposit = accountTransactionalService.deposit(id, amount);
            } catch (PessimisticLockingFailureException e) {
                log.info("Cannot get lock for entity with id={} while depositing", id);
                continue;
            }
            break;
        }

        return deposit;
    }

    public AccountDto withdraw(Long id, BigDecimal amount) {
        AccountDto deposit;

        while (true) {
            try {
                deposit = accountTransactionalService.withdraw(id, amount);
            } catch (PessimisticLockingFailureException e) {
                log.info("Cannot get lock for entity with id={} while withdrawing", id);
                continue;
            }
            break;
        }

        return deposit;
    }

    public MoneyTransferringDto transfer(Long fromId, Long toId, BigDecimal amount) {
        MoneyTransferringDto moneyTransferringDto;
        while (true) {
            try {
                moneyTransferringDto = accountTransactionalService.transfer(fromId, toId, amount);
            } catch (PessimisticLockingFailureException e) {
                log.info("Cannot get lock for entity with id={} while transferring", fromId < toId ? fromId : toId);
                continue;
            }
            break;
        }

        return moneyTransferringDto;
    }
}

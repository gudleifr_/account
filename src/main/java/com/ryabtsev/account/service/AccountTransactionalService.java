package com.ryabtsev.account.service;

import com.ryabtsev.account.db.entity.UserAccountDomain;
import com.ryabtsev.account.db.repository.AccountRepository;
import com.ryabtsev.account.dto.AccountDto;
import com.ryabtsev.account.dto.MoneyTransferringDto;
import com.ryabtsev.account.exception.NotEnoughMoneyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@Transactional
public class AccountTransactionalService {
    @Autowired AccountRepository accountRepository;

    public AccountDto deposit(Long id, BigDecimal amount) {
        UserAccountDomain userAccountDomain = accountRepository.findOne(id);
        userAccountDomain.setBalance(userAccountDomain.getBalance().add(amount));

        return new AccountDto().setId(userAccountDomain.getId()).setBalance(userAccountDomain.getBalance());
    }

    public AccountDto withdraw(Long id, BigDecimal amount) {
        UserAccountDomain userAccountDomain = accountRepository.findOne(id);

        if (userAccountDomain.getBalance().compareTo(amount) < 0) {
            throw new NotEnoughMoneyException();
        }

        userAccountDomain.setBalance(userAccountDomain.getBalance().subtract(amount));

        return new AccountDto().setId(userAccountDomain.getId()).setBalance(userAccountDomain.getBalance());
    }

    public MoneyTransferringDto transfer(Long fromId, Long toId, BigDecimal amount) {
        UserAccountDomain userAccountDomainFrom;
        UserAccountDomain userAccountDomainTo;

        if (fromId < toId) {
            userAccountDomainFrom = accountRepository.findOne(fromId);
            userAccountDomainTo = accountRepository.findOne(toId);
        } else {
            userAccountDomainTo = accountRepository.findOne(toId);
            userAccountDomainFrom = accountRepository.findOne(fromId);
        }

        if (userAccountDomainFrom.getBalance().compareTo(amount) < 0) {
            throw new NotEnoughMoneyException();
        }

        userAccountDomainFrom.setBalance(userAccountDomainFrom.getBalance().subtract(amount));
        userAccountDomainTo.setBalance(userAccountDomainTo.getBalance().add(amount));

        return new MoneyTransferringDto()
                .setFromAccount(new MoneyTransferringDto.FromAccount().setId(userAccountDomainFrom.getId()).setBalance(userAccountDomainFrom.getBalance()))
                .setToAccount(new MoneyTransferringDto.ToAccount().setId(userAccountDomainTo.getId()).setBalance(userAccountDomainTo.getBalance()));
    }
}

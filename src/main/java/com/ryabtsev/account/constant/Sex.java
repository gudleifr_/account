package com.ryabtsev.account.constant;

public enum Sex {
    MALE, FEMALE
}

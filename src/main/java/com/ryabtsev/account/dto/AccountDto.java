package com.ryabtsev.account.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class AccountDto {
    private Long id;
    private BigDecimal balance;
}

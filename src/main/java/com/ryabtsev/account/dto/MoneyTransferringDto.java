package com.ryabtsev.account.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class MoneyTransferringDto {
    private FromAccount fromAccount;
    private ToAccount toAccount;

    @Data
    @Accessors(chain = true)
    public static class FromAccount {
        private Long id;
        private BigDecimal balance;
    }

    @Data
    @Accessors(chain = true)
    public static class ToAccount {
        private Long id;
        private BigDecimal balance;
    }
}

package com.ryabtsev.account.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AccountResponse extends BaseResponse {
    private Long id;
    private BigDecimal balance;

    @Override
    public AccountResponse setError(boolean error) {
         super.setError(error);
         return this;
    }

    @Override
    public AccountResponse setMessage(String message) {
        super.setMessage(message);
        return this;
    }
}

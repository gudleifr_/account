package com.ryabtsev.account.controller.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class MoneyTransferringResponse extends BaseResponse {
    private FromAccount fromAccount;
    private ToAccount toAccount;

    @Data
    @Accessors(chain = true)
    public static class FromAccount {
        private Long id;
        private BigDecimal balance;
    }

    @Data
    @Accessors(chain = true)
    public static class ToAccount {
        private Long id;
        private BigDecimal balance;
    }

    @Override
    public MoneyTransferringResponse setError(boolean error) {
        super.setError(error);
        return this;
    }

    @Override
    public MoneyTransferringResponse setMessage(String message) {
        super.setMessage(message);
        return this;
    }
}

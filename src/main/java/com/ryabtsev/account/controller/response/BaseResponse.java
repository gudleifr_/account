package com.ryabtsev.account.controller.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BaseResponse {
    private boolean error;
    private String message;
}

package com.ryabtsev.account.controller;

import com.ryabtsev.account.controller.request.UserRequest;
import com.ryabtsev.account.converter.UserConverter;
import com.ryabtsev.account.db.entity.UserAccountDomain;
import com.ryabtsev.account.db.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class UserController {
    @Autowired AccountRepository accountRepository;
    @Autowired UserConverter userConverter;

    @RequestMapping(method = RequestMethod.POST, path = "/user")
    public ResponseEntity<Long> create(@RequestBody UserRequest request) {
        if (request == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        UserAccountDomain userAccountDomain = accountRepository.save(userConverter.toUserAccountDomain(request));

        return new ResponseEntity<>(userAccountDomain.getId(), HttpStatus.OK);
    }
}

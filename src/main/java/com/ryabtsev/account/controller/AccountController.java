package com.ryabtsev.account.controller;

import com.ryabtsev.account.controller.response.AccountResponse;
import com.ryabtsev.account.controller.response.MoneyTransferringResponse;
import com.ryabtsev.account.dto.AccountDto;
import com.ryabtsev.account.dto.MoneyTransferringDto;
import com.ryabtsev.account.exception.NotEnoughMoneyException;
import com.ryabtsev.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@Slf4j
public class AccountController {
    @Autowired AccountService accountService;

    @RequestMapping(method = RequestMethod.GET, path = "/deposit/{id}/{amount}")
    public ResponseEntity<AccountResponse> deposit(@PathVariable Long id, @PathVariable BigDecimal amount) {
        if (!positiveAmountCheck(amount)) {
            return new ResponseEntity<>(new AccountResponse().setError(true).setMessage("Amount is negative or zero"), HttpStatus.OK);
        }
        AccountDto accountDto = accountService.deposit(id, amount);

        return new ResponseEntity<>(new AccountResponse().setId(accountDto.getId()).setBalance(accountDto.getBalance()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/withdraw/{id}/{amount}")
    public ResponseEntity<AccountResponse> withdraw(@PathVariable Long id, @PathVariable BigDecimal amount) {
        AccountDto accountDto;
        if (!positiveAmountCheck(amount)) {
            return new ResponseEntity<>(new AccountResponse().setError(true).setMessage("Amount is negative or zero"), HttpStatus.OK);
        }
        try {
            accountDto = accountService.withdraw(id, amount);
        } catch (NotEnoughMoneyException e) {
            return new ResponseEntity<>(new AccountResponse().setError(true).setMessage("Not enough money"), HttpStatus.OK);
        }

        return new ResponseEntity<>(new AccountResponse().setId(accountDto.getId()).setBalance(accountDto.getBalance()), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, path = "/transfer/{fromId}/{toId}/{amount}")
    public ResponseEntity<MoneyTransferringResponse> transfer(@PathVariable Long fromId,
                         @PathVariable Long toId,
                         @PathVariable BigDecimal amount) {
        MoneyTransferringDto moneyTransferringDto;
        if (!positiveAmountCheck(amount)) {
            return new ResponseEntity<>(new MoneyTransferringResponse().setError(true).setMessage("Amount is negative or zero"), HttpStatus.OK);
        }
        try {
            moneyTransferringDto = accountService.transfer(fromId, toId, amount);
        } catch (NotEnoughMoneyException e) {
            return new ResponseEntity<>(new MoneyTransferringResponse().setError(true).setMessage("Not enough money"), HttpStatus.OK);
        }
        return new ResponseEntity<>(new MoneyTransferringResponse()
                .setFromAccount(new MoneyTransferringResponse.FromAccount().setId(moneyTransferringDto.getFromAccount().getId()).setBalance(moneyTransferringDto.getFromAccount().getBalance()))
                .setToAccount(new MoneyTransferringResponse.ToAccount().setId(moneyTransferringDto.getToAccount().getId()).setBalance(moneyTransferringDto.getToAccount().getBalance())),
                HttpStatus.OK);
    }

    private boolean positiveAmountCheck(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) > 0;
    }
}

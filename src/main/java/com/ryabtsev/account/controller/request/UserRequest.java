package com.ryabtsev.account.controller.request;

import com.ryabtsev.account.constant.Sex;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserRequest {
    private String firstName;
    private String secondName;
    private String lastName;
    private Sex sex;
}

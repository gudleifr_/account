package com.ryabtsev.account.db.repository;

import com.ryabtsev.account.db.entity.UserAccountDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;

public interface AccountRepository extends JpaRepository<UserAccountDomain, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    UserAccountDomain findOne(Long id);
}

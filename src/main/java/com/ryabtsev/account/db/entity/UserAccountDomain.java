package com.ryabtsev.account.db.entity;

import com.ryabtsev.account.constant.Sex;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "user_account")
@Data
@Accessors(chain = true)
public class UserAccountDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    @Column
    private String secondName;

    @Column
    private String lastName;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column
    private BigDecimal balance = BigDecimal.ZERO;
}

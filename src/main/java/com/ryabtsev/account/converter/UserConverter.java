package com.ryabtsev.account.converter;

import com.ryabtsev.account.controller.request.UserRequest;
import com.ryabtsev.account.db.entity.UserAccountDomain;
import org.springframework.stereotype.Service;

@Service
public class UserConverter {

    public UserAccountDomain toUserAccountDomain(UserRequest from) {
        if (from == null) return null;

        return new UserAccountDomain()
                .setFirstName(from.getFirstName())
                .setSecondName(from.getSecondName())
                .setLastName(from.getLastName())
                .setSex(from.getSex());
    }
}
